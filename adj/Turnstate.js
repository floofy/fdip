// Turnstate
// =========

// An array of Turnstates constructs the history of a [Map](./Map.html)
class Turnstate {
	constructor(provinces, orders, phase) {
		this.provinces = provinces || [];
		this.orders = orders || [];
		this.phase = phase || null;
	}

	getProvince(province) {
		return this.provinces.find(x => x.abbrv == province);
	}

	setProvinceOwner(province, nation) {
		if (!(province instanceof Province)) province = this.getProvince(province);
		province.owner = nation; 
	}

	setProvinceUnitOwner(province, nation) {
		if (!(province instanceof Province)) province = this.getProvince(province);
		province.unitOwner = nation;
	}
}
