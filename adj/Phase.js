// Phase
// =====

// The class holds information about each phase
class Phase {
	// str : `SEASON,YEAR,TYPE`
	constructor(str) {
		let t = str.split(",");

		this.season = new Season(t[0], t[2]);
		this.year = new Year(t[1]);
	}

	nextPhase() {
		let np = new Phase(this.toString());
		if (this.season.equals(consts.SEASONS.WINTER)) np.year = this.year.nextYear();
		np.season = this.season.nextSeason();

		return np;
	}

	toString() {
		return this.season.s + "," + this.year.toString() + "," + this.season.t;
	}

	equals(phase) {
		return this.season.equals(phase.season) && this.year.equals(phase.year);
	}
}


// Year
// ====
class Year {
	constructor(a) {
		if (isNaN(a)) throw("Parameter is not a valid number.");
		this.year = a;
	}

	nextYear() {
		return new Year(this.year + 1);
	}

	equals(year) {
		return this.year == year.year ||
			this.year == year;
	}

	toString() {
		return "0".repeat(4-String(this.year).length) + String(this.year);
	}
}

// Season
// ======

// The current possible seasons are:
// - SPRING_M
// - SPRING_R
// - FALL_M
// - FALL_R
// - WINTER_B
class Season {
	constructor(a, b) {
		this.SEASON_ORDER = ["SPRING", "FALL", "WINTER"];
		this.SEASON_TYPE = {B: "Builds", M: "Moves", R: "Retreats"}

		if (!this.SEASON_ORDER.includes(a) || !(b in this.SEASON_TYPE)) 
			throw ("Parameters are not valid seasons.")

		this.season = a + "_" + b;
		this.s = a;
		this.t = b;
	}

	nextSeason() {
		if (this.t == "M") return new Season(this.s, "R");
		else if (this.s != "WINTER") 
			return new Season(this.SEASON_ORDER[(this.SEASON_ORDER.find(x=>x==this.s)+1)%3], "M");
		else return new Season("SPRING", "M");
	}

	equals(season) {
		return this.season == season.season ||
			this.season == season ||
			this.s == season;
	} 

	toString() {
		return this.s + " " + this.SEASON_TYPE[this.t];
	}

	isMovementPhase() {
		return this.s != "WINTER";
	}

	isRetreatsPhase() {
		return this.t == "R";
	}

	isBuildsPhase() {
		return this.s == "WINTER";
	}
}