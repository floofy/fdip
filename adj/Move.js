// Order
// =====

// Order is the base class that all other Order classes will be based off of. 
class Order {
	// Every order must have a reference to the map they belong to, a source province, a source unit, and an orderState, at minimum. 
	constructor(map, mvData) {
		this.map = map;
		this.ts = map.current;

		this.src = this.map.getProvince(mvData.src);
		this.unit = this.map.getUnit(mvData.src);

		this.orderState = new OrderState();

		this.comment = mvData.comment || "";
	}

	equals(order) {
		return order.src == this.src;
	}
}


// Move
// ====
class Move extends Order {
	constructor(map, mvData) {
		super(map, mvData);
		this.dst = this.map.getProvince(mvData.dst);
	}

	validate() {
		if (!this.src || !this.dst || !this.unit) return false;

		return true;
	}

	evaluate() {
		console.log("MOVE: evaluate(", this, ")")
		let atk = this.map.adj.attack_strength(this.src, this.dst);

		console.log("    atk:", atk)

		if (this.map.adj.head_to_head(this.src, this.dst)) {
			if (atk <= this.map.adj.defend_strength(this.dst, this.src)) return "FALSE";
		}
		else {
			if (atk <= this.map.adj.hold_strength(this.dst)) return "FALSE";
		}

		return this.map.adj.compareAtkStrengthToAll(this.src, this.dst);
	}

	toString() {
		return this.unit.abbrv + " " + this.src.abbrv + " -> " + this.dst.abbrv;
	}
}




// Support
// =======

// You can either support a unit to move or to hold. 
class Support extends Order {
	constructor(map, mvData) {
		super(map, mvData);
		this.tar = this.map.getProvince(mvData.tar);
		this.dst = this.map.getProvince(mvData.dst);
	}

	validate() {
		if (!this.src.isAdj(this.dst)) return false;

		if (!this.tar.isAdj(this.dst)) return false;

		if (!this.tar.isOccupied()) return false;

		if (!this.map.getCurrentPhase().season.isMovementPhase()) return false;

		return true;
	}

	evaluate() {
		let os = this.map.getOrder(this.tar);

		if (this.tar.equals(this.dst)) {
			// Support hold
			if (os && this.map.adj.is_legal_move(os)) {
				return consts.TRISTATE.FALSE;
			}
		}
		else {
			// Support move
			if (!os || !this.map.adj.is_legal_move(os) || os.dst != this.dst) {
				return consts.TRISTATE.FALSE;
			}
		}

		// Check if cut
		for (var i = 0; i < this.ts.orders.length; i++) {
			if (!this.ts.orders[i] instanceof Move || !this.ts.orders[i].dst.equals(this.src)) continue;

			let attacker = this.ts.orders[i].src;

			if (attacker.owner.equals(this.src.owner)) continue;

			if (this.dst.equals(attacker)) continue;

			if (!this.map.adj.path(attacker, this.src)) continue;

			return consts.TRISTATE.FALSE;
		}

		// Check if dislodged
		if (this.map.adj.dislodged(this.src)) {
			return consts.TRISTATE.FALSE;
		}

		return "TRUE";
	}

	toString() {
		return this.unit.abbrv + " " + this.src.abbrv + " supports " + this.tar.abbrv + " -> " + this.dst.abbrv;
	}
}

// Convoy
// ======

// Fleets may convoy armies to adjacent provinces; convoys may be chained. 
class Convoy extends Order {
	constructor(map, mvData) {
		super(map, mvData);
		this.tar = this.map.getProvince(mvData.tar);
		this.dst = this.map.getProvince(mvData.dst);
	}

	validate() {
		if (this.src.type != "W") return false;
		if (!this.map.getCurrentPhase().season.isMovementPhase()) return false;
		if (this.tar.equals(this.dst)) return false;

		return true;
	}

	evaluate() {
		let os = this.map.getOrder(this.tar);

		if (!os || !os instanceof Move || !os.dst.equals(this.dst)) {
			return consts.TRISTATE.FALSE;
		}

		if (this.map.adj.dislodged(this.src)) {
			return consts.TRISTATE.FALSE;
		}

		if (os.orderState.resolution == "FALSE" && os.orderState.evalState == "RESOLVED") {
			return "FALSE";
		}

		return "TRUE";
	}

	toString() {
		return this.unit.abbrv + " " + this.src.abbrv + " convoys " + this.tar.abbrv + " -> " + this.dst.abbrv;
	}
}

/** Build */
class Build extends Order {
	constructor(map, unit, src) {
		super(map, unit, src);
		if (!(unit instanceof Unit)) {
			this.unit = ((unit == "A") ? consts.UNITS.ARMY : ((unit == "F") ? consts.UNITS.FLEET : null));
		}
	}

	validate() {
		if (!this.src.center) return false;

		if (this.src.isOccupied()) return false;

		if (this.unit.equals(consts.UNITS.ARMY) && this.src.type == "water") return false;

		if (this.unit.equals(consts.UNITS.FLEET) && this.src.type == "land") return false;

		if (!this.map.getCurrentPhase().season.isBuildsPhase()) return false;

		return true;
	}

	evaluate() {
		return consts.TRISTATE.TRUE;
	}
}

/** Disband */
class Disband extends Order {
	constructor(map, unit, src) {
		super(map, unit, src);
	}

	validate() {
		if (!this.src.isOccupied()) return false;

		if (!this.map.getCurrentPhase().isRetreatsPhase() && !this.map.getCurrentPhase().isBuildsPhase()) return false;

		return true;
	}

	evaluate() {
		return consts.TRISTATE.TRUE;	
	}
}



/* Result classes */
class OrderState {
	constructor() {
		this.evalState = "UNRESOLVED";
		this.resolution = null;
		// this.dislodged = "FALSE";
	}
}


const classesMapping = {
	"Move": Move,
	"Support": Support,
	"Convoy": Convoy,
	"Build": Build,
	"Disband": Disband
}