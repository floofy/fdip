// Unit
// ====
class Unit {
	constructor(name, abbrv) {
		this.name = name;
		this.abbrv = abbrv;
	}

	equals(unit) {
		return (unit.abbrv == this.abbrv && unit.name == this.name) ||
			unit == this.abbrv || unit == this.name;
	}
}