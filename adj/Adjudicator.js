// Adjudicator class
// =================

// It should be initialized ONCE per game
class Adjudicator {
	constructor(map) {
		this.map = map;
		this.ol = map.current.orders;
		this.ts = map.current;

		this.deps = [];
		this.deps_n;

		this.retreats = [];
	}

	// run_adjudicator
	// ---------------
	run_adjudicator() {
		this.ol = this.map.current.orders;

		this.adjudicate_all();
		this.compute_retreats();

		console.log(this.ol);
		console.log(this.retreats)
	}

	adjudicate_all() {
		this.deps_n = 0;

		for (var i = 0; i < this.ol.length; i++) {
			this.resolve(this.ol[i]);
		}
	}

	compute_retreats() {
		for (var i = 0; i < this.ts.provinces.length; i++) {
			let pi = this.ts.provinces[i];

			if (!this.dislodged(pi)) continue;

			this.retreats.push({
				province: pi,
				retreat_locations: []
			})

			let neighbors = (pi.unit.equals("Army")) ? pi.getLandAdj() : pi.getSeaAdj();

			for (var j = 0; j < neighbors.length; j++) {
				let t2 = neighbors[j];

				if (this.can_retreat(pi, t2)) this.retreats.find(x => x.province.equals(pi)).retreat_locations.push(t2);
			}
		}
	}

	getBuilds() {
		let ret = [];
		for (var i = 0; i < consts.NATIONS.length; i++) {
			if (consts.NATIONS[i].name) {
				ret.push({name: consts.NATIONS[i].name, centers: 0, units: 0});
			}
		}

		for (var i = 0; i < this.ts.provinces.length; i++) {
			let province = this.ts.provinces[i];
			if (province.center && province.owner.name) {
				ret.find(x => x.name == province.owner.name).centers += 1;
			}
			if (province.unit.fullName) {
				ret.find(x => x.name == province.owner.name).units += 1;
			}
		}

		return ret;
	}

	can_retreat(src, dst) {
		if (this.hold_strength(dst) > 0) return false;

		if (this.head_to_head(src, dst, true)) return false;

		for (var i = 0; i < this.ordersList.length; i++) {
			if (this.ordersList[i] instanceof Move && this.ordersList[i].dst.equals(dst) && !this.ordersList[i].src.equals(src)) {
				if (this.prevent_strength(this.ordersList[i].src, dst) > 0) return false;
			}
		}

		return true;
	}

	dislodged(province) {
		console.log("ADJUDICATOR: dislodged(" + province.abbrv + ")")

		if (!province.isOccupied()) return false;

		let o = this.getOrder(province);

		if (o && o instanceof Move && this.resolve(o) == "TRUE") return false;

		for (var i = 0; i < this.ol.length; i++) {
			if (this.ol[i] instanceof Move && this.ol[i].dst.equals(province) && this.resolve(this.ol[i]) == "TRUE") {
				return true;
			}
		}

		return false;
	}

	// resolve
	// -------
	resolve(order) {
		console.log("ADJUDICATOR: resolve(", order , ")")
		if (order.orderState.evalState == "RESOLVED") {
			return order.orderState.resolution;
		}

		if (order.orderState.evalState == "GUESSING") {
			this.deps.push(order);
			this.deps_n += 1;
			return order.orderState.resolution;
		}

		let deps_n_old = this.deps_n;

		order.orderState.resolution = "FALSE";
		order.orderState.evalState = "GUESSING";

		let res1 = this.adjudicate(order);

		if (this.deps_n == deps_n_old) {
			// Order does NOT depend on a guess
			if (order.orderState.evalState != "RESOLVED") {
				order.orderState.resolution = res1;
				order.orderState.evalState = "RESOLVED";
			}

			return res1;
		}

		if (!this.deps[deps_n_old].equals(order)) {
			/* Order depends on a guess that is NOT our own */
			this.deps.push(order);
			this.deps_n++;
			order.orderState.resolution = res1;
			return res1;
		}

		/* Result depends on our guess; reset the dependencies */
		for (var i = deps_n_old; i < this.deps_n; i++) {
			this.deps[i].orderState.evalState = "UNRESOLVED";
		}

		this.deps_n = deps_n_old;

		/* Try the other guess */
		order.orderState.resolution = "TRUE";

		order.orderState.evalState = "GUESSING";

		let res2 = this.adjudicate(order);

		if (res1 == res2) {
			/* The cycle has a unique solution; reset the dependencies and return */
			for (var i = deps_n_old; i < this.deps_n; i++) {
				this.deps[i].orderState.evalState = "UNRESOLVED";
			}

			this.deps_n = deps_n_old;

			order.orderState.resolution = res1;
			order.orderState.evalState = "RESOLVED";

			return res1;
		}

		this.backup_rule(order);

		return this.resolve(order);
	}

	adjudicate(order) {
		console.log("ADJUDICATOR: Beginning adjudicate(" + order.src.abbrv + ")")

		if (!order.validate()) {
			return "FALSE";
		}

		return order.evaluate();
	}

	getOrder(province) {
		return this.ol.find(x => x.src.equals(province));
	}

	attack_strength(src, dst) {
		console.log("ADJUDICATOR: attack_strength(" + src.abbrv + ", " + dst.abbrv + ")");

		if (!this.path(src, dst)) return 0;

		let o = this.getOrder(dst);

		let successful_move = o instanceof Move && !o.dst.equals(src) && this.resolve(o) == "TRUE";

		// If the destination is NOT occupied or if the destination SUCCESSFULLY moves out, we do
		// not have to account for a unit there
		if (!dst.isOccupied() || successful_move) {
			return 1 + this.successful_supports(src, dst, []);
		}

		// You cannot dislodge yourself
		if (dst.owner.equals(src.owner)) {
			return 0;
		}

		return 1 + this.successful_supports(src, dst, dst.owner);
	}

	successful_supports(src, dst, excluded) {
		let ret = 0;

		if (!Array.isArray(excluded)) excluded = [excluded];

		for (var i = 0; i < this.ol.length; i++) {
			let sup_nation = this.ol[i].src.owner;
			if (this.ol[i] instanceof Support && 
					this.ol[i].tar.equals(src) && 
					this.ol[i].dst.equals(dst) && 
					!excluded.some(x => x.equals(sup_nation)) && 
					this.resolve(this.ol[i]) == "TRUE") {
				ret += 1;
			}
		}

		return ret;
	}

	path(src, dst) {
		console.log("ADJUDICATOR: path(" + src.abbrv + ", " + dst.abbrv + ")")
		if (!src.isOccupied()) return false;

		if (src.unit.equals("Fleet")) return src.isUnitAdj(dst);

		let can_reach_ret = src.isUnitAdj(dst);

		let convoy_bool = this.convoy_path(src, dst);

		if (!can_reach_ret || convoy_bool) {
			return convoy_bool;
		}

		return can_reach_ret;
	}

	convoy_path(src, dst) {
		console.log("ADJUDICATOR: convoy_path(" + src.abbrv + ", " + dst.abbrv + ")")

		if (!src.isCoastal() || !dst.isCoastal()) return false;

		if (src.unit.equals("Fleet")) return false;

		this.visited = [];

		this.cpa = [];

		let ret = this.convoy_path_r(src, dst, src);

		this.visited = [];

		return ret;
	}

	convoy_path_r(src, dst, cur) {
		console.log("ADJUDICATOR: convoy_path_r(" + src.abbrv + ", " + dst.abbrv + ", " + cur.abbrv + ")")
		for (var i = 0; i < cur.adj.length; i++) {
			let t = cur.adj[i];

			if (t.equals(dst) && !cur.equals(src)) {
				this.cpa.push(cur)
				return true;
			}


			if (t.isCoastal() || this.visited.some(x => x == t.abbrv) || !t.isOccupied()) continue;


			this.visited.push(cur.abbrv);

			let o = this.getOrder(t);

			if (!o || !(o instanceof Convoy) || !o.tar.equals(src) || !o.dst.equals(dst) || this.resolve(o) == "FALSE") continue;

			if (this.convoy_path_r(src, dst, t)) {
				return true;
			}
		}

		return false;
	}

	defend_strength(src, dst) {
		return 1 + this.successful_supports(src, dst, []);
	}

	hold_strength(src) {
		if (!src.isOccupied()) return 0;

		let o = this.getOrder(src);

		if (o && this.is_legal_move(o, null)) {
			return this.resolve(o) == "TRUE" ? 0 : 1;
		}

		let strength = 1;
		for (var i = 0; i < this.ol.length; i++) {
			if (this.ol[i] instanceof Support && this.ol[i].dst.equals(src) && this.ol[i].tar.equals(src) && this.resolve(this.ol[i]) == "TRUE") {
				strength++;
			}
		}

		return strength;
	}

	is_legal_move(order) {
		if (!(order instanceof Move)) return false;

		if (!order.src.isOccupied()) return false;

		return true;
	}

	compareAtkStrengthToAll(src, dst) {
		for (var i = 0; i < this.ol.length; i++) {
			if (this.ol[i] instanceof Move && this.ol[i].dst.equals(dst) && !this.ol[i].src.equals(src)) {
				let atk = this.atk_strength_vs(src, dst, this.ol[i].src.owner);

				if (atk <= this.prevent_strength(this.ol[i].src, dst)) return "FALSE";
			}
		}

		return "TRUE";
	}

	atk_strength_vs(src, dst, opponent) {
		if (!this.path(src, dst)) return 0;

		let o = this.getOrder(dst);

		let successful_move = o && o instanceof Move && this.resolve(o).equals(consts.TRISTATE.TRUE);

		if (!dst.isOccupied() || successful_move) {
			return 1 + this.successful_supports(src, dst, opponent);
		}
		else {
			return 1 + this.successful_supports(src, dst, [opponent, dst.owner]);
		}
	}

	prevent_strength(src, dst) {
		if (!this.path(src, dst)) return 0;

		if (this.head_to_head(src, dst, false) && this.resolve(this.getOrder(dst)).equals(consts.TRISTATE.TRUE)) {
			return 0;
		}

		return 1 + this.successful_supports(src, dst, []);
	}

	head_to_head(src, dst, retreat) {
		let o2 = this.getOrder(dst);

		if (!o2 || !o2 instanceof Move || !o2.dst.equals(src) || this.convoy_path(dst, src)) {
			return false;
		}

		if (retreat) return true;

		let o1 = this.getOrder(src);

		return o1 && o1 instanceof Move && o1.dst.equals(dst) && !this.convoy_path(src, dst);
	}
}