// Map
// ===

// The map should hold all information about the game. 
class Map {
	// province_data : 
	// ```
	// [{
	// 	name: String, 
	// 	abbrv: String, 
	// 	center: [Number, Number], 
	// 	unit: [Number, Number], 
	// 	type: String, 
	// 	river: Number, 
	// 	adj: [String]
	// }]
	// ```

	// game_data : 
	// ```
	// {
	// 	variant_name: String,
	// 	variant_description: String,
	// 	rule_options: {
	// 		builds: String,
	// 	},
	// 	startingPhase: String,
	// 	unitTypes: [[String,String]],
	// 	provinceTypes: [String],
	// 	moveTypes: [String],
	// 	nations: [{
	// 		name: String, 
	// 		abbrv: String, 
	// 		rgb: [Number, Number, Number, Number], 
	// 		compl: [Number, Number, Number, Number]
	// 	}],
	// }
	// ```

	// game_state : 
	// ```
	// {
	// 	variant_name: String,
	// 	phase: String,
	// 	state: [{
	// 		name: String, 
	// 		owner: String,
	// 		unit: String,
	// 		unitOwner: String, 
	// 		center: bool
	// 	}]
	// }
	// ```

	// orders_data : 
	// ```
	// [{
	// 	type: String, 
	// 	src: String, 
	// 	dst: String, 
	// 	comment: String
	// }]
	// ```

	// Constructs the Map class, passing in province_data, game_data, game_state, and orders_data
	constructor(province_data, game_data, game_state, orders_data) {
		// Initialize [Turnstate](./Turnstate.html)
		this.turnStates = [];
		this.current = new Turnstate();

		// Load and parse game data
		this.loadGame(game_data);
		this.loadProvinces(province_data, game_state);
		this.loadOrders(orders_data);

		// Initialize [Adjudicator](./adjudicator.html)
		this.adj = new Adjudicator(this);
	}

	// loadGame
	// --------

	// Load basic game information based off of static game data
	loadGame(game_data) {
		// Load basic game details
		this.variant_name = game_data.variant_name;
		this.variant_description = game_data.variant_description;
		this.rule_options = game_data.rule_options;

		// Initialize the first starting phase—this will *not* go into the current turnstate **if**
		// the game_state has an entry for [Phase](./Phase.html). 
		this.starting_phase = new Phase(game_data.startingPhase);

		// Initialize [Unit](./Unit.html) constants, provinceTypes array, and moveTypes array, that will later
		// be checked for validity. 
		this.UNITS = [];
		this.PVS = game_data.provinceTypes;
		this.MVS = game_data.moveTypes;
		for (var i = 0; i < game_data.unitTypes.length; i++) {
			let gi = game_data.unitTypes[i];
			this.UNITS.push(new Unit(gi[0], gi[1]));
		}

		// Initialize the [Nation](./nation.html) constants
		this.NATIONS = [];
		for (var i = 0; i < game_data.nations.length; i++) {
			this.NATIONS.push(new Nation(game_data.nations[i]));
		}

	}

	// loadProvinces
	// -------------

	//  Parse province data, containing map coordinates, adjacencies, province type, etc, and convert it into the map Province class. 
	loadProvinces(province_data, gs) {
		for (var i = 0; i < province_data.length; i++) {
			let pdi = province_data[i];
			let psi = gs.state.find(x => x.name == pdi.abbrv);

			let unit = (psi) ? this.UNITS.find(x => x.equals(psi.unit)) : null;
			let owner = (psi) ? this.NATIONS.find(x => x.abbrv == psi.owner) : null;
			let unitOwner = (psi) ? this.NATIONS.find(x => x.abbrv == psi.unitOwner) : null;

			// Checks:
			if (!this.PVS.includes(pdi.type)) continue;

			let p = new Province(pdi.name, pdi.abbrv, pdi.adj, pdi.type, unit, pdi.center, owner, unitOwner);
			this.current.provinces.push(p);
		}

		// Convert adjacencies from strings to Province references
		for (var i = 0; i < this.current.provinces.length; i++) {
			for (var j = 0; j < this.current.provinces[i].adj.length; j++) {
				for (var k = 0; k < this.current.provinces[i].adj[j].length; k++) {
					this.current.provinces[i].adj[j][k] = this.getProvince(this.current.provinces[i].adj[j][k]);

				}
			}
		}

		// load phase
		this.current.phase = (gs.phase) ? new Phase(gs.phase) : this.starting_phase;
	}

	// loadOrders
	// ----------

	// Load all orders. This currently **wipes** the current cache of orders. 
	loadOrders(od) {
		this.current.orders = [];
		

		for (var i = 0; i < od.length; i++) {
			if (this.MVS.includes(od[i].type) && 
				this.getProvince(od[i].src).unitOwner.equals(od[i].owner)) {
				// Creates a new [Order](./Move.html)
				this.current.orders.push(new classesMapping[od[i].type](this, od[i]));
			}
		}
	}


	// adjudicate
	// ----------

	// Adjudicate the orders in `this.current.orders`
	adjudicate() {
		console.log("MAP: Beginning adjudication");

		this.adj.run_adjudicator();

		let ret = [];

		for (var i = 0; i < this.current.orders.length; i++) {
			let coi = this.current.orders[i];
			ret.push(coi.toString() + " " + coi.orderState.resolution + " - Comment: " + coi.comment)
		}

		console.log(ret)

		console.log("MAP: Finished adjudication");
	}

	// Utility functions
	getProvince(name) {
		return this.current.provinces.find(x => x.abbrv == name);
	}

	getUnit(name) {
		return this.getProvince(name).unit;
	}

	getOrder(province) {
		return this.current.orders.find(x => x.src.equals(province));
	}

	// isOccupied(name) {
	// 	return this.getProvince(name).isOccupied();
	// }

	// getConvoyPath(src, dst) {
	// 	if (!(src instanceof Province)) src = this.getProvince(src);
	// 	if (!(dst instanceof Province)) dst = this.getProvince(dst);

	// 	let isValid = this.adj.convoy_path(src, dst);

	// 	if (isValid) {
	// 		return this.adj.cpa;
	// 	}
	// 	else return null;
	// }

	// getLastTurnState() {
	// 	return this.turnStates[this.turnStates.length-1];
	// }

	// // For after it adjudicates: convert data into an appropriate JSON format
	// updateState() {
	// 	this.turnStates.push(_.clone(this.current))

	// 	this.current = new Turnstate(_.clone(this.current.provinces));

	// 	let lastTurnState = this.getLastTurnState();

	// 	for (var i = 0; i < lastTurnState.orders.length; i++) {
	// 		let o = lastTurnState.orders[i];
	// 		let os = o.orderState;

	// 		if (os.evalState.equals(consts.TRISTATE.RESOLVED) && os.resolution.equals(consts.TRISTATE.TRUE)) {
	// 			if (o instanceof Move) {
	// 				o.dst.unit = o.src.unit;
	// 				o.src.unit = consts.UNITS.NONE;

	// 				if ((lastTurnState.phase.season.equals(consts.SEASONS.FALL) || lastTurnState.phase.season.equals(consts.SEASONS.FALL_R)) || !o.center) lastTurnState.setProvinceOwner(o.dst.abbrv, o.src.owner);
	// 				lastTurnState.setProvinceUnitOwner(o.dst.abbrv, o.src.unitOwner)
	// 				o.src.unitOwner = consts.NATIONS.find(x => x.abbrv == "n");
	// 			}
	// 			else if (o instanceof Build) {
	// 				o.src.unit = o.unit;
	// 			}
	// 			else if (o instanceof Disband) {
	// 				o.src.unit = consts.UNITS.NONE;
	// 			}
	// 		}
	// 	}

	// 	// Next season
	// 	this.current.phase = lastTurnState.phase.nextPhase();
	// 	if (this.current.phase.season.equals(consts.SEASONS.SPRING_R) || this.current.phase.season.equals(consts.SEASONS.FALL_R)) {
	// 		if (this.adj.retreats.length < 1) {
	// 			this.current.phase = this.current.phase.nextPhase();
	// 		}
	// 	}
	// 	if (this.current.phase.season.equals(consts.SEASONS.WINTER)) {
	// 		if (this.adj.getBuilds().length < 1) {
	// 			this.current.phase = this.current.phase.nextPhase();
	// 		}
	// 	}

	// 	this.current.orders = [];
	// 	this.adj = new Adjudicator(this);

	// 	return this.getState();
	// }

	// getState() {
	// 	let state = [];

	// 	for (var i = 0; i < this.current.provinces.length; i++) {
	// 		let pi = this.current.provinces[i];

	// 			let province = {
	// 				"name": pi.abbrv,
	// 				"owner": pi.owner.abbrv,
	// 				"unit": pi.unit.abbrv,
	// 				"center": pi.center,
	// 				"unitOwner": pi.unitOwner.abbrv
	// 			};

	// 			state.push(province);
	// 	}

	// 	return state;
	// }

	// getOrderResults() {
	// 	let orderResults = [];

	// 	for (var i = 0; i < this.getLastTurnState().orders.length; i++) {
	// 		let tso = this.getLastTurnState().orders[i];
	// 		let order = {
	// 			order: tso.toString(),
	// 			result: tso.orderState.resolution.text.toUpperCase(),
	// 			dislodged: "Dislodged: " + tso.orderState.dislodged.text
	// 		};

	// 		orderResults.push(order);
	// 	}

	// 	return orderResults;
	// }

	getCurrentPhase() {
		return this.current.phase;
	}

	getCurrentSeason() {
		return this.getCurrentPhase().season;
	}
}

let global;
try {
  global = Function('return this')() || (42, eval)('this');
} catch(e) {
  global = window;
}