// Province
// ========

// Holds all the information about a single province.
class Province {
	// name : String
	// abbrv : String
	// adj : [String]
	// type: String
	// unit: Unit
	// center: bool
	// owner: Nation
	// unitOwner : Nation
	constructor(name, abbrv, adj, type, unit, center, owner, unitOwner) {
		this.name = name;
		this.abbrv = abbrv;
		this.adj = adj;
		this.type = type;

		this.unit = unit;
		this.center = center;
		this.owner = owner;
		this.unitOwner = unitOwner;
	}

	isOccupied() {
		if (this.unit) return true;
		return false;
	}

	isAdj(province) {
		return this.isLandAdj(province) || this.isSeaAdj(province);
	}

	isLandAdj(province) {
		return this.adj[0].some(x=>x.equals(province));
	}

	isSeaAdj(province) {
		return this.adj[1].some(x=>x.equals(province));
	}

	isUnitAdj(province) {
		if (this.unit.equals("Army")) return this.isLandAdj(province);
		else if (this.unit.equals("Fleet")) return this.isSeaAdj(province);
	}

	getLandAdj() {
		return this.adj[0];
	}

	getSeaAdj() {
		return this.adj[1];
	}

	isCoastal() {
		return this.adj.some(x => x.type == "W") && this.type != "W";
	}

	isLandlocked() {
		return !this.isCoastal() && this.type != "W";
	}

	stripCoast() {
		return this.province.abbrv.substring(0, this.province.abbrv.search("/"));
	}

	equals(province) {
		return (this.name == province.name && this.abbrv == province.abbrv) || 
			this.name == province || this.abbrv == province;
	}

	equalsNC(province) {
		return this.equals(province) || this.stripCoast() == province.stripCoast();
	}
}