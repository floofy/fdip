// Nation
// ======

// Each nation should hold basic data and only be initialized ONCE.
class Nation {
	constructor(nd) {
		this.name = nd.name;
		this.abbrv = nd.abbrv;
		this.rgb = nd.rgb;
		this.compl = nd.compl
	}

	equals(nation) {
		return (this.name == nation.name && this.abbrv == nation.abbrv) ||
			this.name == nation || this.abbrv == nation;
	}
}