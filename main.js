function getJSON(filename) {
	let myObj;
	let xmlHTTP = new XMLHttpRequest();
	xmlHTTP.onreadystatechange = function() {
		if (this.readyState == 4 && this.status == 200) {
			myObj = JSON.parse(this.responseText);
		}
	}

	xmlHTTP.open("GET", filename, false);
	xmlHTTP.send();

	return myObj;
}

let d = getJSON("./1-d.json");
let p = getJSON("./1-p.json");
let s = getJSON("./1-s.json");
let o = getJSON("./1-o.json");

let map = new Map(p, d, s, o);

map.adjudicate();